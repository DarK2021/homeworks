let elem = document.getElementById('elem');
const parent = document.getElementById('root')

// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
elem.addEventListener('focus', function () {
    elem.style.border = '1px solid #00ff00';
    // elem.classList.add('focus');
    elem.style.outline = 'none';
  });

// Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
elem.addEventListener('blur', function () {
  if (elem.value === ''){
    elem.style.border = '1px solid black';
    // elem.classList.remove('focus');
  } else if(Math.sign(elem.value) === -1) {
    const span = document.createElement('span');
    elem.style.border =' 1px solid #FF0000';
    // elem.classList.remove('focus');
    // elem.classList.remove('invalid');
    span.innerText = 'Please enter correct price.'
    span.after(elem);
  } else {
    const span = document.createElement('span');
    const button = document.createElement('button');
    button.innerText = 'x'
    span.innerText = `Текущая цена: ${elem.value}`;
    span.appendChild(button);
    elem.before(span);
    button.addEventListener('click', ()=>removeElement(span)) 
    // elem.style.backgroundColor = '#00ff00';
    // span.before(elem)
    // parent.appendChild(span)
    // parent.insertAdjacentElement("afterend", span)
    // button.addEventListener('click', removeElem(span))
  }
  });
  

// При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
function removeElement (span) {
	span.remove();
  elem.value = '';
};

// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
// elem.onblur = function() {
//   if (!elem.value.includes > '0') { 
//     elem.style.borderColor=' 1px solid #FF0000';
//     elem.value.add('invalid');
//     innerHTML = 'Please enter correct price.'
//     elem.focus();
//   }
// };

//   // Вопрос: Опишите своими словами, как Вы понимаете, что такое обработчик событий.
// //   Ответ: обработчик событий - это функция, которая обрабатывает, или откликается на событие. С его помощью, можно указать, например, что делать при клике по кнопке, или что делать при наборе текста в текстовом поле.